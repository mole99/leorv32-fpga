// SPDX-FileCopyrightText: © 2020 Bruno Levy <https://github.com/BrunoLevy/learn-fpga>
// SPDX-License-Identifier: BSD-3-Clause

// Generate HDMI signal from VGA signal
module GFX_hdmi(
    input wire 	      pixel_clk,    // pixel clock
    input wire 	      pixel_clk_x5, // 5 times pixel clock freq (used by TMDS serializer)
                                    // The TMDS serializers operate at (pixel_clock_freq * 10),
                                    // but we use DDR mode, hence (pixel_clock_freq * 5).
    input wire [7:0]  R,
    input wire [7:0]  G,
    input wire [7:0]  B,
    input wire 	      hsync,
    input wire 	      vsync,
    input wire        draw_area,

    output wire [3:0] gpdi_dp // HDMI signals, blue, green, red, clock 	
                              // dgpi_dn generated by pins (see, e.g., ulx3s.lpf)
);
   
   // RGB TMDS encoding
   // Generate 10-bits TMDS red,green,blue signals. Blue embeds HSync/VSync in its 
   // control part.
   wire [9:0] TMDS_R, TMDS_G, TMDS_B;
   TMDS_encoder encode_R(.clk(pixel_clk), .VD(R), .CD(2'b00)        , .VDE(draw_area), .TMDS(TMDS_R));
   TMDS_encoder encode_G(.clk(pixel_clk), .VD(G), .CD(2'b00)        , .VDE(draw_area), .TMDS(TMDS_G));
   TMDS_encoder encode_B(.clk(pixel_clk), .VD(B), .CD({vsync,hsync}), .VDE(draw_area), .TMDS(TMDS_B));

   // Modulo-5 clock divider.
   reg [4:0] TMDS_mod5=1;
   wire      TMDS_shift_load = TMDS_mod5[4];
   always @(posedge pixel_clk_x5) TMDS_mod5 <= {TMDS_mod5[3:0],TMDS_mod5[4]};
   
   // Shifters
   // Every 5 clocks, we get a fresh R,G,B triplet from the TMDS encoders,
   // else we shift.
   reg [9:0] TMDS_shift_R=0, TMDS_shift_G=0, TMDS_shift_B=0;
   always @(posedge pixel_clk_x5) begin
      TMDS_shift_R <= TMDS_shift_load ? TMDS_R : {2'b00,TMDS_shift_R[9:2]};
      TMDS_shift_G <= TMDS_shift_load ? TMDS_G : {2'b00,TMDS_shift_G[9:2]};
      TMDS_shift_B <= TMDS_shift_load ? TMDS_B : {2'b00,TMDS_shift_B[9:2]};	
   end

   // DDR serializers: they send D0 at the rising edge and D1 at the falling edge.
`ifndef BENCH_OR_LINT
 `ifdef ULX3S
   ODDRX1F ddr_R (.D0(TMDS_shift_R[0]), .D1(TMDS_shift_R[1]), .Q(gpdi_dp[2]), .SCLK(pixel_clk_x5), .RST(1'b0));
   ODDRX1F ddr_G (.D0(TMDS_shift_G[0]), .D1(TMDS_shift_G[1]), .Q(gpdi_dp[1]), .SCLK(pixel_clk_x5), .RST(1'b0));
   ODDRX1F ddr_B (.D0(TMDS_shift_B[0]), .D1(TMDS_shift_B[1]), .Q(gpdi_dp[0]), .SCLK(pixel_clk_x5), .RST(1'b0));
 `endif
`endif
   
   // The pixel clock is sent through the fourth differential pair.
   assign gpdi_dp[3] = pixel_clk;

endmodule
