// SPDX-FileCopyrightText: © 2022 Leo Moser <https://codeberg.org/mole99>
// SPDX-License-Identifier: GPL-3.0-or-later

`timescale 1ns / 1ps

module ulx3s_top (
    input clk_25mhz,

    input  ftdi_txd,
    output ftdi_rxd,

    input  [6:0] btn,
    output [7:0] led,

    output wire [3:0] gpdi_dp, // HDMI signals, blue, green, red, clock 	
                               // dgpi_dn generated by pins (see, e.g., ulx3s.lpf)

    // SPI Flash
    `ifndef SYNTHESIS
    output flash_clk,
    `endif
    output flash_csn,
    output flash_mosi,
    input  flash_miso,
    output flash_holdn,
    output flash_wpn
);
    localparam int FREQUENCY = 25_000_000;
    localparam int BAUDRATE  = 9600;
    localparam int NUM_CORES = 8;
    localparam int SVGA = 1;

    logic video_clk;
    logic video_clk5;

    `ifdef SYNTHESIS
    GFX_PLL GFX_PLL(
    .pclk (clk_25mhz),         // the board's clock
    .pixel_clk (video_clk),    // pixel clock
    .pixel_clk_x5 (video_clk5)  // 5 times pixel clock freq (used by TMDS serializer)
                          // The TMDS serializers operate at (pixel_clock_freq * 10), 
                          // but we use DDR mode, hence (pixel_clock_freq * 5).
    );
    `else
    assign video_clk = clk_25mhz;
    assign video_clk5 = clk_25mhz;
    `endif

    // SVGA graphics
    logic hsync;
    logic vsync;
    logic enable;
    logic [ 3: 0] paint_r;
    logic [ 3: 0] paint_g;
    logic [ 3: 0] paint_b;

    leosoc #(
        .FREQUENCY  (FREQUENCY),
        .BAUDRATE   (BAUDRATE),
        .NUM_CORES  (NUM_CORES),
        .SVGA       (SVGA)
    ) leosoc (
        .clk        (clk_25mhz),
        .video_clk  (video_clk),
        
        .reset      (!btn[0]),

        .uart_rx    (ftdi_txd),
        .uart_tx    (ftdi_rxd),

        .blink      (led[7]),
        
        // SVGA graphics
        .hsync      (hsync),
        .vsync      (vsync),
        .enable     (enable),
        .paint_r    (paint_r),
        .paint_g    (paint_g),
        .paint_b    (paint_b),
        
        // SPI signals
        .sck (flash_clk),
        .sdo (flash_mosi),
        .sdi (flash_miso),
        .cs  (flash_csn)
    );
    
    `ifdef SYNTHESIS
    wire flash_clk;
    USRMCLK u1 (
        .USRMCLKI(flash_clk),
        .USRMCLKTS(1'b0) // no tristate
    );
    `endif

    assign flash_wpn = 1'b0; // Write Protect
    assign flash_holdn = 1'b1; // No reset

    GFX_hdmi GFX_hdmi (
        .pixel_clk  (video_clk),    // pixel clock
        .pixel_clk_x5(video_clk5), // 5 times pixel clock freq (used by TMDS serializer)
                                // The TMDS serializers operate at (pixel_clock_freq * 10), 
                                // but we use DDR mode, hence (pixel_clock_freq * 5).
        .R          ({paint_r, {4{paint_r[0]}}}),
        .G          ({paint_g, {4{paint_g[0]}}}),
        .B          ({paint_b, {4{paint_b[0]}}}),
        .hsync      (hsync),
        .vsync      (vsync),
        .draw_area  (enable),

        .gpdi_dp    (gpdi_dp) // HDMI signals, blue, green, red, clock 	
                              // dgpi_dn generated by pins (see, e.g., ulx3s.lpf)
    );

endmodule
