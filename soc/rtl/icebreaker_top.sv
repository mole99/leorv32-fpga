// SPDX-FileCopyrightText: © 2022 Leo Moser <https://codeberg.org/mole99>
// SPDX-License-Identifier: GPL-3.0-or-later

`timescale 1ns / 1ps

module icebreaker_top (
    input CLK,

    // On-board
    input RX,
    output logic TX,

    input BTN_N,
    output logic LEDR_N,
    output logic LEDG_N,

    // PMOD 2
    input BTN1,
    input BTN2,
    input BTN3,

    // PMOD DVI
    output logic       dvi_clk,    // DVI pixel clock
    output logic       dvi_hsync,  // DVI horizontal sync
    output logic       dvi_vsync,  // DVI vertical sync
    output logic       dvi_de,     // DVI data enable
    output logic [3:0] dvi_r,      // 4-bit DVI red
    output logic [3:0] dvi_g,      // 4-bit DVI green
    output logic [3:0] dvi_b,      // 4-bit DVI blue

    // SPI Flash
    output FLASH_SCK,
    output FLASH_SSB,
    output FLASH_IO0,
    input  FLASH_IO1,
    output FLASH_IO2,
    output FLASH_IO3
);

    localparam int FREQUENCY = 12_000_000;
    localparam int BAUDRATE  = 9600;
    localparam int NUM_CORES = 2;
    localparam int SVGA = 0;

    logic locked;
    
    logic clk;
    logic video_clk;

`ifdef SYNTHESIS
    // * Given input frequency:        12.000 MHz
    // * Requested output frequency:   40.000 MHz
    // * Achieved output frequency:    39.750 MHz

    SB_PLL40_2_PAD #(
        .FEEDBACK_PATH("SIMPLE"),
        .DIVR         (4'b0000),     // DIVR =  0
        .DIVF         (7'b0110100),  // DIVF = 52
        .DIVQ         (3'b100),      // DIVQ =  4
        .FILTER_RANGE (3'b001)       // FILTER_RANGE = 1
    ) uut (
        .LOCK  (locked),
        .RESETB(1'b1),
        .BYPASS(1'b0),

        .PACKAGEPIN(CLK),
        .PLLOUTGLOBALA(clk),  // buffered input clock
        .PLLOUTGLOBALB(video_clk)  // synthesized clock
    );
`else
    assign video_clk = CLK;
    assign clk = CLK;
    assign locked = 1'b1;
`endif

    logic reset;

    SB_GB reset_buffer (
        .USER_SIGNAL_TO_GLOBAL_BUFFER(!BTN_N || !locked),
        .GLOBAL_BUFFER_OUTPUT(reset)
    );
    
    // SVGA graphics
    logic hsync;
    logic vsync;
    logic enable;
    logic [ 3: 0] paint_r;
    logic [ 3: 0] paint_g;
    logic [ 3: 0] paint_b;

    leosoc #(
        .FREQUENCY(FREQUENCY),
        .BAUDRATE (BAUDRATE),
        .NUM_CORES(NUM_CORES),
        .SVGA(SVGA)
    ) leosoc (
        .clk(clk),
        .video_clk(video_clk),
        .reset (reset),

        .uart_rx(RX),
        .uart_tx(TX),

        .blink(LEDG_N),

        // SVGA graphics
        .hsync      (hsync),
        .vsync      (vsync),
        .enable     (enable),
        .paint_r    (paint_r),
        .paint_g    (paint_g),
        .paint_b    (paint_b),
        
        // SPI signals
        .sck (FLASH_SCK),
        .sdo (FLASH_IO0),
        .sdi (FLASH_IO1),
        .cs  (FLASH_SSB)
    );

    assign FLASH_IO2 = 1'b0; // Write Protect
    assign FLASH_IO3 = 1'b1; // No reset

    // DVI Pmod output
    SB_IO #(
        .PIN_TYPE(6'b010100)  // PIN_OUTPUT_REGISTERED
    ) dvi_signal_io[14:0] (
        .PACKAGE_PIN({dvi_hsync, dvi_vsync, dvi_de, dvi_r, dvi_g, dvi_b}),
        .OUTPUT_CLK(video_clk),
        .D_OUT_0({hsync, vsync, enable, paint_r, paint_g, paint_b}),
        .D_OUT_1()
    );

    // DVI Pmod clock output: 180° out of phase with other DVI signals
    SB_IO #(
        .PIN_TYPE(6'b010000)  // PIN_OUTPUT_DDR
    ) dvi_clk_io (
        .PACKAGE_PIN(dvi_clk),
        .OUTPUT_CLK(video_clk),
        .D_OUT_0(1'b0),
        .D_OUT_1(1'b1)
    );

    assign LEDR_N = 1'b0;

endmodule
