// SPDX-FileCopyrightText: © 2022 Leo Moser <https://codeberg.org/mole99>
// SPDX-License-Identifier: GPL-3.0-or-later

`timescale 1ns / 1ps

module leosoc #(
    parameter int FREQUENCY = 12_000_000,
    parameter int BAUDRATE  = 9600,
    parameter int NUM_CORES = 1,
    parameter int SVGA = 0
) (
    input clk,
    input video_clk,
    input reset,

    input        uart_rx,
    output logic uart_tx,

    output logic blink,

    // SVGA graphics
    output hsync,
    output vsync,
    output enable,
    output [ 3: 0] paint_r,
    output [ 3: 0] paint_g,
    output [ 3: 0] paint_b,
    
    // SPI signals
    output sck,
    output sdo,
    input  sdi,
    output cs
);

    // Configuration

    localparam SOC_ADDRW = 32;
    
    localparam NUM_WMASKS = 4;
    localparam DATA_WIDTH = 32;
    localparam WRAM_ADDR_WIDTH = 10;
    localparam VRAM_ADDR_WIDTH = 10; // TODO

    localparam WRAM_MASK        = 8'h00;
    localparam VRAM_MASK        = 8'h01;
    localparam SPI_FLASH_MASK   = 8'h02;
    localparam UART_MASK        = 8'h0A;
    localparam BLINK_MASK       = 8'h0F;

    // Synchronization

    logic uart_rx_sync;

    synchronizer #(
        .FF_COUNT(3)
    ) synchronizer (
        .clk(clk),
        .resetn(!reset),
        .in(uart_rx),

        .out(uart_rx_sync)
    );

    // ----------------------------------
    //           LeoRV32 Core
    // ----------------------------------

    logic [31: 0] mem_addr;
    logic [31: 0] mem_wdata;
    logic [ 3: 0] mem_wmask;
    logic         mem_wstrb;
    logic [31: 0] mem_rdata;
    logic         mem_rstrb;
    logic         mem_done;
    
    // Peripherals have no latency (except SPI Flash)
    assign mem_done = soc_spi_flash_sel ? spi_flash_done && mem_rstrb : mem_rstrb || mem_wstrb;

    core_wrapper #(
        .NUM_CORES(NUM_CORES),
        .RESET_ADDR(32'h02000000 + 24'h200000),
        .ADDR_WIDTH(SOC_ADDRW)
    ) core_wrapper (
        .clk    (clk),
        .reset  (reset || !spi_flash_initialized), // TODO necessary?
        
        .mem_addr (mem_addr),
        .mem_wdata(mem_wdata),
        .mem_wmask(mem_wmask),
        .mem_wstrb(mem_wstrb),
        .mem_rdata(mem_rdata),
        .mem_rstrb(mem_rstrb),
        .mem_done (mem_done)
    );
    
    logic soc_wram_sel;
    logic soc_vram_sel;
    logic soc_spi_flash_sel;
    logic soc_uart_sel;
    logic soc_blink_sel;
    
    assign soc_wram_sel         = mem_addr[31:24] == WRAM_MASK;
    assign soc_vram_sel         = mem_addr[31:24] == VRAM_MASK;
    assign soc_spi_flash_sel    = mem_addr[31:24] == SPI_FLASH_MASK;
    assign soc_uart_sel         = mem_addr[31:24] == UART_MASK;
    assign soc_blink_sel        = mem_addr[31:24] == BLINK_MASK;
    
    logic soc_wram_sel_del;
    logic soc_vram_sel_del;
    logic soc_spi_flash_sel_del;
    logic soc_uart_sel_del;
    logic soc_blink_sel_del;
    
    always_ff @(posedge clk) begin
        if (reset) begin
            soc_wram_sel_del        <= 1'b0;
            soc_vram_sel_del        <= 1'b0;
            soc_spi_flash_sel_del   <= 1'b0;
            soc_uart_sel_del        <= 1'b0;
            soc_blink_sel_del       <= 1'b0;
        end else begin
            soc_wram_sel_del        <= soc_wram_sel;
            soc_vram_sel_del        <= soc_vram_sel;
            soc_spi_flash_sel_del   <= soc_spi_flash_sel;
            soc_uart_sel_del        <= soc_uart_sel;
            soc_blink_sel_del       <= soc_blink_sel;
        end
    end

    // WRAM Memory
    
    // Memory Port 1 - R/W
    logic wram_web0;
    logic [NUM_WMASKS-1:0] wram_wmask0;
    logic [WRAM_ADDR_WIDTH-1:0] wram_addr0;
    logic [DATA_WIDTH-1:0] wram_din0;
    logic [DATA_WIDTH-1:0] wram_dout0;
    
    // Memory Port 2 - R
    logic [WRAM_ADDR_WIDTH-1:0] wram_addr1;
    logic [DATA_WIDTH-1:0] wram_dout1;

    sram #(
        .ADDR_WIDTH(WRAM_ADDR_WIDTH),
        .INIT_F("")
    ) wram (
        // Port 0: RW
        .clk0  (clk),
        .csb0  (1'b0),
        .web0  (wram_web0),
        .wmask0(wram_wmask0),
        .addr0 (wram_addr0),
        .din0  (wram_din0),
        .dout0 (wram_dout0),

        // Port 1: R
        .clk1 (clk),
        .csb1 (1'b0),
        .addr1(wram_addr1),
        .dout1(wram_dout1)
    );
    
    // SoC read data
    logic [DATA_WIDTH-1:0] mem_rdata_memory;
    logic [DATA_WIDTH-1:0] mem_rdata_vram;
    
    // Connect WRAM
    assign wram_web0        = !(mem_wstrb && soc_wram_sel);
    assign wram_wmask0      = mem_wmask;
    assign wram_addr0       = mem_addr >> 2;
    assign wram_din0        = mem_wdata;
    assign mem_rdata_memory = wram_dout0;

    always_comb begin
        // VRAM
        if (soc_vram_sel_del) begin
            mem_rdata = mem_rdata_vram;
        // SPI Flash
        end else if (soc_spi_flash_sel_del) begin
            mem_rdata = spi_flash_rdata;
        // UART
        end else if (soc_uart_sel_del) begin
            mem_rdata = uart_reg;
        // Blink
        end else if (soc_blink_sel_del) begin
            mem_rdata = {32{blink}};
        // WRAM
        end else begin
            mem_rdata = mem_rdata_memory;
        end
    end

    // Blinky

    always @(posedge clk, posedge reset) begin
        if (reset) begin
            blink <= 1'b0;
        end else if (soc_blink_sel && mem_wstrb) begin
            blink = mem_wdata[0];
        end
    end

    // Uart

    logic mem_rstrb_delayed;
    always_ff @(posedge clk, posedge reset) begin
        if (reset) begin
            mem_rstrb_delayed <= 1'b0;
        end else begin
            mem_rstrb_delayed <= mem_rstrb;
        end
    end

    logic rx_flag;
    always_ff @(posedge clk, posedge reset) begin
        if (reset) begin
            rx_flag  <= 1'b0;
        end else begin
            if (!rx_done_delayed && rx_done) rx_flag <= 1'b1;
            else if (soc_uart_sel && mem_rstrb) rx_flag <= 1'b0;
        end
    end
    
    logic [DATA_WIDTH-1: 0] uart_reg;
    assign uart_reg = {rx_flag, tx_busy, {22{1'b0}}, rx_received};

    logic [7:0] rx_received;
    logic rx_done;
    logic rx_done_delayed;

    always_ff @(posedge clk) begin
        if (reset) begin
            rx_done_delayed <= 1'b0;
        end else begin
            rx_done_delayed <= rx_done;
        end
    end

    my_uart_rx #(
        .FREQUENCY(FREQUENCY),
        .BAUDRATE (BAUDRATE)
    ) my_uart_rx (
        .clk    (clk),
        .rst    (reset),
        .rx     (uart_rx_sync),
        .data   (rx_received),
        .valid  (rx_done)
    );

    logic tx_busy;

    my_uart_tx #(
        .FREQUENCY(FREQUENCY),
        .BAUDRATE (BAUDRATE)
    ) my_uart_tx (
        .clk    (clk),
        .rst    (reset),
        .data   (mem_wdata[7:0]),
        .start  (soc_uart_sel && mem_wstrb),
        .tx     (uart_tx),
        .busy   (tx_busy)
    );

    // *** SVGA ***

    generate

    if (SVGA) begin

        logic [DATA_WIDTH-1: 0] vram_dout0;
        assign mem_rdata_vram = vram_dout0;

        svga_gen_top #(
            .INIT_F("images/color_test_chart.hex")
        )
        svga_gen_top (
            .reset,

            // VRAM Port
            .clk,
            .mem_addr,
            .mem_wdata,
            .mem_wmask,
            .vram_dout0,
            .soc_vram_sel,

            // SVGA Signals
            .video_clk,
            .hsync,
            .vsync,
            .enable,
            .paint_r,
            .paint_g,
            .paint_b
        );

    end

    endgenerate
    
    // SPI Flash
    
    logic [DATA_WIDTH-1:0] spi_flash_rdata;
    logic spi_flash_done;
    logic spi_flash_initialized;
    
`ifdef SYNTHESIS
    spi_flash spi_flash_inst (
        .clk,
        .reset,

        .addr_in    (mem_addr[23:0]),      // address of word
        .data_out   (spi_flash_rdata),              // received word
        .strobe     (soc_spi_flash_sel && mem_rstrb),    // start transmission
        .done       (spi_flash_done),               // pulse, transmission done
        .initialized(spi_flash_initialized),        // initial cmds sent

        // SPI signals
        .sck,
        .sdo,
        .sdi,
        .cs
    );
    
`else
    // TODO SPI Flash does not want to simulate correctly
    //      use this as a workaround
    
    localparam INIT_F = "firmware/firmware.hex";
    localparam OFFSET = 24'h200000;
    
	// 16 MB (128Mb) Flash
	reg [7:0] memory [0:16*1024*1024-1];
	initial begin
		$readmemh(INIT_F, memory, OFFSET);
	end
	
	logic [7:0] counter;
	
    always_ff @(posedge clk, posedge reset) begin
        if (reset) begin
            spi_flash_done <= 1'b0;
            spi_flash_initialized = 1'b1;
            counter <= '0;
        end else begin
            spi_flash_done <= 1'b0;
            counter <= '0;
	        if (soc_spi_flash_sel && mem_rstrb) begin
	            spi_flash_rdata <= {memory[(mem_addr[23:2]<<2) + 3], memory[(mem_addr[23:2]<<2) + 2], memory[(mem_addr[23:2]<<2) + 1], memory[(mem_addr[23:2]<<2)]};
	            
	            counter <= counter +1;
	            
	            if (counter > 100)
                spi_flash_done <= 1'b1;
            end
	    end
    end

`endif

endmodule
