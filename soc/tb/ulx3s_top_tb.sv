// SPDX-FileCopyrightText: © 2022 Leo Moser <https://codeberg.org/mole99>
// SPDX-License-Identifier: GPL-3.0-or-later

`timescale 1 ns / 1 ps

module ulx3s_top_tb;

    parameter int CLOCK_PERIOD_NS = 40;  // 25 MHz clock
    parameter int SER_BIT_PERIOD_NS = 104167; // 9600 baud

    initial begin
        $dumpfile("ulx3s_top_tb.fst");
        $dumpvars(0, ulx3s_top_tb);
        //for (int i = 0; i < 32; i++) $dumpvars(0, ulx3s_top.leosoc.core_wrapper.cpus[0].leorv32_inst.leorv32_regs_inst.regs[i]);
    end

    logic [7:0] leds;

    logic ser_tx;
    logic ser_rx;

    logic [6:0] buttons;

    logic clk = 0;
    always #(CLOCK_PERIOD_NS / 2) clk = !clk;

    logic resetn;

    assign buttons[0]   = resetn;
    assign buttons[6:1] = '1;

    initial begin
        resetn = 1'b0;
        ser_rx = 1'b1;

        $display("Starting simulation.");

        #(CLOCK_PERIOD_NS * 2);
        resetn = 1'b1;
        
        #(CLOCK_PERIOD_NS * 300);
        send_byte_ser("!");
        #(CLOCK_PERIOD_NS * 60000);

        #(CLOCK_PERIOD_NS * 60000);
        #(CLOCK_PERIOD_NS * 60000);
        #(CLOCK_PERIOD_NS * 60000);
        #(CLOCK_PERIOD_NS * 60000);
        #(CLOCK_PERIOD_NS * 60000);
        #(CLOCK_PERIOD_NS * 60000);
        #(CLOCK_PERIOD_NS * 60000);
        #(CLOCK_PERIOD_NS * 60000);
        #(CLOCK_PERIOD_NS * 60000);
        #(CLOCK_PERIOD_NS * 60000);
        #(CLOCK_PERIOD_NS * 60000);
        #(CLOCK_PERIOD_NS * 60000);

        $display("Completed simulation.");
        $finish;
    end

    logic FLASH_SCK;
    wire  FLASH_IO0;
    logic FLASH_IO1;
    logic FLASH_SSB;

    spiflash #(
        .INIT_F("firmware/firmware.hex"),
        .OFFSET(24'h200000)
    ) spiflash_inst (
        .csb    (FLASH_SSB),
        .clk    (FLASH_SCK),
        .io0    (FLASH_IO0), // MOSI
        .io1    (FLASH_IO1), // MISO
        .io2    (),
        .io3    ()
    );

    ulx3s_top ulx3s_top (
        .clk_25mhz(clk),

        .ftdi_rxd(ser_tx),
        .ftdi_txd(ser_rx),

        .btn(buttons),

        .led(leds),
        
        .flash_clk  (FLASH_SCK),
        .flash_csn  (FLASH_SSB),
        .flash_mosi (FLASH_IO0),
        .flash_miso (FLASH_IO1),
        .flash_holdn(),
        .flash_wpn  ()
    );
    
    logic [7:0] recv_byte = 0;

    always @(negedge ser_tx) begin
        read_byte_ser;
    end

    task automatic read_byte_ser;
        #(SER_BIT_PERIOD_NS / 2);  // Wait half baud
        if ((ser_tx == 0)) begin

            #SER_BIT_PERIOD_NS;

            // Read data LSB first
            for (int j = 0; j < 8; j++) begin
                recv_byte[j] = ser_tx;
                #SER_BIT_PERIOD_NS;
            end

            if ((ser_tx == 1)) begin

                //$write(colors::Green);
                $display("leorv32 --> uart: 0x%h '%c'", recv_byte, recv_byte);
                //$write(colors::None);
            end
        end
    endtask

    task automatic send_byte_ser(input bit [7:0] data);
        //$write(colors::Blue);
        $display("uart --> leorv32: 0x%h '%c'", data, data);
        //$write(colors::None);

        // Start bit
        ser_rx = 0;
        #SER_BIT_PERIOD_NS;

        // Send data LSB first
        for (int i = 0; i < 8; i++) begin
            ser_rx = data[i];
            #SER_BIT_PERIOD_NS;
        end

        // Stop bit
        ser_rx = 1;
        #SER_BIT_PERIOD_NS;
    endtask

endmodule
